package getTitle;

import domainExtraction.GetDomain;

public class RefinedTitle {

	// Call this method when title is only Single word.
	// This method will replace Single word title with domain name.
	public String getTitle(String title, String url) {
		if (!title.contains(" ")) {
			title = new GetDomain().getUrlDomainName(url);
		}
		return title;
	}

	public static void main(String[] args) {
		System.out.println(new RefinedTitle().getTitle("Sourcepep".trim(), "www.leadlake.com"));
	}
}
