package domainExtraction;

import java.net.URL;

import com.google.common.net.InternetDomainName;

public class GetDomain {

	public String getUrlDomainName(String url) 
	{
		try {
			InternetDomainName fullDomainName = InternetDomainName.from(new URL(url).getHost()).topPrivateDomain();
			if(url.matches("^(https?)?(://)?(www\\.)?.+\\.blogspot\\..+"))
			{
				return fullDomainName.parts().get(1);
			}
			return fullDomainName.parts().iterator().next();
		} 
		catch (Exception e) 
		{
			String domainName = new String(url);

			int index = domainName.indexOf("://");

			if (index != -1)
				domainName = domainName.substring(index + 3);
			index = domainName.indexOf('/');

			if (index != -1)
				domainName = domainName.substring(0, index);
			try
			{
				InternetDomainName fullDomainName = InternetDomainName.from(domainName).topPrivateDomain();

				if(url.matches("^(https?)?(://)?(www\\.)?.+\\.blogspot\\..+"))
				{
					return fullDomainName.parts().get(1);
				}
				return fullDomainName.parts().iterator().next();
			}
			catch(Exception e1)
			{
				String[] str;
				int i=0;
				String mainDomain;

				domainName = domainName.replaceFirst("^www.*?\\.", ""); 
				str =	domainName.split("\\."); 
				i = str.length-1; 
				if(str[i].length()!=2)
					i ++; 
				if(i < 3) 
				{ 
					try 
					{ 
						mainDomain=domainName.substring(0,
								domainName.indexOf(".")); 
					} 
					catch(StringIndexOutOfBoundsException	e2) 
					{ 
						mainDomain=domainName; 
					}

				} 
				else 
				{ 
					try
					{
						mainDomain=domainName.substring(domainName.indexOf(".")+1,
								domainName.length());
						mainDomain=mainDomain.substring(0,
								mainDomain.indexOf(".")); 
					} 
					catch(StringIndexOutOfBoundsException	e3) 
					{ 
						mainDomain=domainName.substring(0,
								domainName.indexOf(".")); 
					}
				}
				return mainDomain;
			}
		}
	}
}
