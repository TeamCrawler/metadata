package values;

public interface ParkedPageValues {
	final static int htmlLengtLimit = 50;
	final static int numOfLinksLimit = 3;
	final static int descriptionLengthLimit = 7; //atleast 7 words.
}
