package main;

import java.io.IOException;

import org.jsoup.nodes.Document;

import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import bean.MetaData;
import getTitle.RefinedTitle;
import oddMetaData.ExtractMetaData;
import values.ParkedPageValues;

public class Start implements ParkedPageValues {
	public static void main(String[] args) throws IOException, BoilerpipeProcessingException {
		boolean flag = false;
		ExtractMetaData object = new ExtractMetaData();

		int id = 0;
		MongoClient mongoClient = new MongoClient("localhost", 27017);
		// Now connect to your databases
		DB db = mongoClient.getDB("crawler");
		DBCollection coll = db.getCollection("sampleCrawldata");
		DBCollection metaTesting = db.getCollection("metaTesting");

		DBObject obj;
		DBObject insertObj;
		DBCursor cursor = coll.find(); // If we don not have query then keep
										// brackets blank. It will fetch all
										// records
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		while (cursor.hasNext()) {
			// System.out.println(cursor.next());
			insertObj = new BasicDBObject();
			obj = cursor.next();
			String link = obj.get("link").toString();
			insertObj.put("_id", ++id);
			insertObj.put("link", link);
			Document doc;
			try {
				doc = object.jsoupDocument(link);

				int htmlLength = new ExtractMetaData().htmlLength(doc.toString());
				if (htmlLength <= htmlLengtLimit) {
					insertObj.put("status", "HTML Length is less then 50");
					flag = true;
				}

				String domainName = new ExtractMetaData().getUrlDomainName(link);
				int numOfLinks = new ExtractMetaData().domainPageCheck(doc, domainName);

				MetaData metaDataObj;
				if (numOfLinks < numOfLinksLimit) {
					insertObj.put("status", "Num of links are less then 3");
					flag = true;
				}

				System.out.println("Flag value is "+flag);
				if (flag == false) {
					metaDataObj = object.getMeta(doc, link);
					insertObj.put("status", "success");
					insertObj.put("newTitle", metaDataObj.getTitle());
					insertObj.put("newDescription", metaDataObj.getDescription());
					insertObj.put("newKeyword", metaDataObj.getKeywords());
					insertObj.put("newMainContent", metaDataObj.getMainContent());

				}
				flag = false;
				metaTesting.insert(insertObj);
				System.out.println(insertObj);
			} catch (Exception e) {
				insertObj.put("status", e.getClass().getName() + " : " + e.getMessage());
				metaTesting.insert(insertObj);
			}
		}

	}
}
