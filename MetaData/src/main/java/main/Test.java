package main;

import java.io.IOException;
import java.util.Map;

import org.jsoup.nodes.Document;

import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;

import bean.MetaData;
import oddMetaData.ExtractMetaData;
import values.ParkedPageValues;

public class Test implements ParkedPageValues{

	public static void main(String[] args) throws IOException, BoilerpipeProcessingException{

		Map<String, String> map;
		ExtractMetaData object = new ExtractMetaData();

		String link = "https://www.go4worldbusiness.com/inquiries/send/products/630575/dual-15-speaker-driver-units";
		Document doc = object.jsoupDocument(link);
		int htmlLength = new ExtractMetaData().htmlLength(doc.toString());
		if (htmlLength <= htmlLengtLimit) {
			System.out.println("Wrong link. HTML length not satisfied");
			System.exit(0);
		}
		String domainName = new ExtractMetaData().getUrlDomainName(link);
		int numOfLinks = new ExtractMetaData().domainPageCheck(doc, domainName);

		MetaData metaDataObj;
		if (numOfLinks >= numOfLinksLimit) {
			metaDataObj = object.getMeta(doc, link);
			System.out.println("Title:- " + metaDataObj.getTitle());
			System.out.println("Description:- " + metaDataObj.getDescription());
			/*System.out.println("Keywords:- " + metaDataObj.getKeywords());
			System.out.println("MainContent:- " + metaDataObj.getMainContent());*/
			System.out.println("Article:- " + metaDataObj.getArticleExtractor());
			System.out.println("Evrything:- " + new ExtractMetaData().Datafilters(metaDataObj.getKeepEvrything()));
			// System.out.println(map.get("description"));
		} else {
			System.out.println("Wrong link.Domain link count is less.");
		}
	}
}
