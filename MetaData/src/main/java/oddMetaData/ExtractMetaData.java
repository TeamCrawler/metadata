package oddMetaData;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.extractors.ArticleExtractor;
import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;

import bean.MetaData;
import getTitle.RefinedTitle;
import values.ParkedPageValues;

public class ExtractMetaData implements ParkedPageValues {
	Document htmlDocument;
	String keywords = "", description = "", mainContent = "", title = "", onlyDescription = "", keepEvrything = "",
			articleExtractor = "";
	String onlyWordsDescription = "";
	Map<String, String> map;
	Set<String> set;
	MetaData metaDataObj = new MetaData();

	public Document jsoupDocument(String link) throws IOException {

		return (Jsoup.connect(link)
				.userAgent(
						"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
				.referrer("http://www.google.com").header("Accept-Language", "en").timeout(180000).followRedirects(true)
				.get());
	}

	public MetaData getMeta(Document htmlDoc, String link) throws BoilerpipeProcessingException {
		metaDataObj = new MetaData();
		title = htmlDoc.title();
		map = new HashMap<String, String>();
		for (Element meta : htmlDoc.select("meta")) {
			if (meta.attr("name").toLowerCase().contentEquals("keyword")
					|| meta.attr("name").toLowerCase().contentEquals(("keywords"))) {
				keywords += " " + meta.attr("content");
			}
			if (meta.attr("name").toLowerCase().contentEquals("description")
					|| meta.attr("name").toLowerCase().contentEquals("descriptions")) {
				description += " " + meta.attr("content");
			}
			// On some webpages meta description is given as follows,
			// <meta description = "some desription"/>
			// To fetch these description following line is written.
			onlyDescription = onlyDescription + meta.attr("description");
		}

		mainContent = getArticle(htmlDoc.toString());
		articleExtractor = mainContent;
		keepEvrything = getEverything(htmlDoc.toString());

		//To remove html tags and nonbreaking spaces.
		//This is needed as this output is given to phone finder.
		keepEvrything = keepEvrything.replaceAll("<[^>]*?>", "").replaceAll("(^\\h*)|(\\h*$)", "");

		title = Datafilters(title).trim();
		title = new RefinedTitle().getTitle(title, link).trim();
		title = Datafilters(title).trim();

		description = Datafilters(description);
		onlyWordsDescription = description.replaceAll("[^a-zA-Z\n ]", " ").replaceAll(" +", " ");
		keywords = Datafilters(keywords);

		if (onlyWordsDescription.isEmpty() || length(onlyWordsDescription) < descriptionLengthLimit) {
			description = Datafilters(onlyDescription);
			onlyWordsDescription = description.replaceAll("[^a-zA-Z\n ]", " ").replaceAll(" +", " ");
		}

		if (onlyWordsDescription.isEmpty() || length(onlyWordsDescription) < descriptionLengthLimit) {
			description = Datafilters(mainContent);
			onlyWordsDescription = description.replaceAll("[^a-zA-Z\n ]", " ").replaceAll(" +", " ");
		}
		if (onlyWordsDescription.isEmpty() || length(onlyWordsDescription) < descriptionLengthLimit) {
			description = Datafilters(keepEvrything);
			onlyWordsDescription = description.replaceAll("[^a-zA-Z\n ]", " ").replaceAll(" +", " ");
		}

		if (onlyWordsDescription.isEmpty() || length(onlyWordsDescription) < descriptionLengthLimit) {
			description = "";
		}

		if (description.startsWith("."))
			description = description.substring(1, description.length());

		metaDataObj.setTitle(title);
		metaDataObj.setDescription(description);
		metaDataObj.setKeywords(keywords);
		metaDataObj.setMainContent(mainContent);
		metaDataObj.setArticleExtractor(articleExtractor);
		metaDataObj.setKeepEvrything(keepEvrything);

		title = "";
		description = "";
		keywords = "";
		mainContent = "";
		articleExtractor = "";
		keepEvrything = "";
		return metaDataObj;
	}

	// 1. remove all the html tags present in description.
	// 2. Removed all special characters other than(#@:.).
	// 3. Removes non-breakng space(Unicode value 160 in java).
	// 4. If multiple occurences of .#@:/ is present then replace them with only
	// one occurence.
	// 5. Removed unwanted spaces.
	// 6. Removed Start and end spaces.
	public String Datafilters(String metaData) {
		metaData = StringUtils.stripAccents(metaData);
		metaData = metaData.replaceAll("<[^>]*?>", "").replaceAll("[^a-zA-Z0-9#@:/.\n ]", " ").replaceAll(" +", " ")
				.replaceAll("(^\\h*)|(\\h*$)", "").replaceAll("( *\\.)+", ".").replaceAll("( *\\#)+", "#")
				.replaceAll("( *\\@)+", "@").replaceAll("( *\\:)+", ":").replaceAll("( *\\/)+", "/")
				.replaceAll("\n", " ").replaceAll(" +", " ").trim();
		return metaData;
	}

	// 1. DataFilters for duplicate title and description
	public String DuplicateDatafilters(String metaData) {
		metaData = StringUtils.stripAccents(metaData);
		metaData = metaData.replaceAll("<[^>]*?>", "").replaceAll("[^a-zA-Z#@:/.\n ]", " ").replaceAll(" +", " ")
				.replaceAll("(^\\h*)|(\\h*$)", "").replaceAll("( *\\.)+", ".").replaceAll("( *\\#)+", "#")
				.replaceAll("( *\\@)+", "@").replaceAll("( *\\:)+", ":").replaceAll("( *\\/)+", "/")
				.replaceAll("\n", " ").replaceAll("\\s*", "").trim();
		return metaData;
	}

	// 1. If text length < 5 then content is useless
	public int length(String text) {
		return text.split(" +").length;
	}

	// 1. If html length < 50 then website is useless
	public int htmlLength(String text) {
		return text.split("\n").length;
	}

	// 1. If number of domain links are less than 3 then useless links
	public int domainPageCheck(Document doc, String domain) throws MalformedURLException {
		set = new HashSet<>();
		Elements elements = doc.select("a");
		for (Element ele : elements) {

			if (ele.attr("abs:href").trim().contains(domain)) {
				if (!ele.attr("abs:href").trim().toLowerCase().contentEquals(domain.toLowerCase())) {
					if (set.size() == 4) {
						break;
					}
					set.add(ele.attr("abs:href"));
				}
			}
		}
		return set.size();
	}

	/**
	 * Will extract the mainDomain from url and return i/p:
	 * http://london.creiglist.co.uk/job/abc/ o/p: http://london.creiglist.co.uk
	 * 
	 * @param url
	 * @return mainDomain
	 * @throws MalformedURLException
	 */
	public String getUrlDomainName(String url) throws MalformedURLException {
		return new URL(url).getProtocol() + "://" + new URL(url).getHost();
	}

	// 1. Article Extractor from text;
	public String getArticle(String text) throws BoilerpipeProcessingException {

		return ArticleExtractor.INSTANCE.getText(text).replaceAll("<[^>]*?>", "");

	}

	// 1. Keep everything Extractor from text;
	public String getEverything(String text) throws BoilerpipeProcessingException {

		return KeepEverythingExtractor.INSTANCE.getText(text).replaceAll("<[^>]*?>", "");

	}
}