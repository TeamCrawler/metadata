package bean;

public class MetaData {
	String title;
	String keywords;
	String description;
	String mainContent;
	String keepEvrything;
	String articleExtractor;
	
	public String getKeepEvrything() {
		return keepEvrything;
	}
	public void setKeepEvrything(String keepEvrything) {
		this.keepEvrything = keepEvrything;
	}
	public String getArticleExtractor() {
		return articleExtractor;
	}
	public void setArticleExtractor(String articleExtractor) {
		this.articleExtractor = articleExtractor;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMainContent() {
		return mainContent;
	}
	public void setMainContent(String mainContent) {
		this.mainContent = mainContent;
	}

}
